<?
date_default_timezone_set('Asia/Karachi');

$repo_url		= ''; // Git repo full url.
$branch_name	= ''; // The branch which you need to deploy.
$username		= ''; // gitlab or any other service username.
$password		= ''; // gitlab or any other service password.
$secret_key		= ''; // this key would be passed via get. It stops the direct exection of file.
$git_push_via_ftp = true;

// ========= DO NOT EDIT BELOW THIS LINE ============

ob_start();

$current_path = getcwd().'/';

if(preg_match('/[^a-z_\-0-9]/i', $password))
{
  die("Only alphanumeric password is accepted.");
}

if ( $secret_key == '' )
{
	die('Please mention the secret key in file.');
}
 
if ( ! isset($_GET['secret_key']) || $_GET['secret_key'] != $secret_key )
{
	die('You don\'t got the key? Get the hell otta here. i dont wanna see your nasty face again.');
}


$check_get_installed = shell_exec("cd ".$current_path."; git status 2>&1");

preg_match('/git: not found/i', $check_get_installed, $matches, PREG_OFFSET_CAPTURE, 3);

if ( count($matches) > 0 )
{
	die('Please install git on your server. For ubuntu servers type: "apt-get install git"');
}

$check_git_version = shell_exec("cd ".$current_path."; git --version 2>&1");

preg_match('/git version ([0-9.]+)/', $check_git_version, $matches);

$git_version = $matches[1];
$git_version_arr = explode('.', $git_version);
$git_major_version = $git_version_arr[0];

$repo_url = str_replace('https://', '', $repo_url);
$repo_url = str_replace('http://', '', $repo_url);

if ( $git_push_via_ftp == true )
{
	$output1 = shell_exec("cd ".$current_path."; git add . 2>&1");
	print $output1.'<br><br>';

	if ( $git_major_version == 2 )
	{
		$output2 = shell_exec("cd ".$current_path."; git -c user.email='ftp@user.com' -c user.name='FTP User' commit -m 'Direct FTP changes commit on ".date('r')."' 2>&1");
		print $output2.'<br><br>';
	}
	else
	{
		$output2 = shell_exec("cd ".$current_path."; git commit -m 'Direct FTP changes commit on ".date('r')."' 2>&1");
		print $output2.'<br><br>';
	}
	
	$output3 = shell_exec("cd ".$current_path."; git push -u origin ".$branch_name." 2>&1");
	print $output3.'<br><br>';
}	 
	 
$output = shell_exec("cd ".$current_path."; git checkout ".$branch_name."; git pull 2>&1");
print $output.'<br><br>';

preg_match('/Authentication failed/i', $output, $matches, PREG_OFFSET_CAPTURE, 3);

if ( count($matches) > 0 )
{
	die('Authentication failed. Please check your username/password.');
}

preg_match('/Not a git repository/i', $output, $matches, PREG_OFFSET_CAPTURE, 3);

if ( count($matches) > 0 )
{
	print 'Git repo does not exists. Cloning repo to this directory.<br>';
 
	//  git clean -xdf;
	
	$command = "cd ".$current_path."; git init; git remote add origin https://".$username.":".$password."@".$repo_url."; git checkout ".$branch_name."; git pull; git branch --set-upstream-to=origin/".$branch_name." ".$branch_name."; git checkout ".$branch_name." 2>&1";
	
//	print $command;
	
	print shell_exec( $command );
}

$log_content = ob_get_clean();

print $log_content;

$filename = getcwd().'/git_deployment.log';

$log_info = "
#################################
date: ".date('r')."	
#################################	

	"; 

$log_content = $log_info . $log_content;

// In our example we're opening $filename in append mode.
// The file pointer is at the bottom of the file hence
// that's where $somecontent will go when we fwrite() it.
if (!$handle = fopen($filename, 'a'))
{
	echo "Cannot open file ($filename)";
	exit;
}

// Write $somecontent to our opened file.
if (fwrite($handle, $log_content) === FALSE)
{
	echo "<br><br>Cannot write to file ($filename)";
	exit;
}

die();
?>